
# Description

This docker definition provides you a JBoss 5.1.0.GA Server based on the java:openjdk-6-jdk docker image. To make this work some fixes are made to the original JBoss server download file (see Dockerfile).

# Usage

The JBoss server is configured with an additional, external deployment folder under `/app`. So all you need to do is mount a local folder into the filesystem of the container by using the `-v`-flag. 


```
$ docker run -ti -v ${PWD}:/app -p 8080:8080 tomkramer/jboss:5.1.0.GA
```

`-v ${PWD}:/app` mounts your current directory to the `/app` directory to the container as depyoment folder.

`-ti` starts the container in interactive mode. You can then end the jboss-server (and container) with `CTRL+c` in your terminal.

`-p 8080:8080` cares for binding the container port 8080:8080 to your local port 8080. You might change this if you need to.