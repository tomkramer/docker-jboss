FROM java:openjdk-6-jdk

WORKDIR /app

RUN curl -L -o jboss.zip https://sourceforge.net/projects/jboss/files/JBoss/JBoss-5.1.0.GA/jboss-5.1.0.GA-jdk6.zip/download \
    && unzip jboss.zip -d /opt/ \
    && rm jboss.zip
RUN ln -s /opt/jboss-5.1.0.GA /opt/jboss \
    && cd /opt/jboss-5.1.0.GA/bin \
    && chmod +x *.sh

# BUGFIX needed since certain JDK6 Version
RUN sed -i 's/<constructor><parameter><inject bean="BootstrapProfileFactory" property="attachmentStoreRoot"/<constructor><parameter class="java.io.File"><inject bean="BootstrapProfileFactory" property="attachmentStoreRoot"/g' /opt/jboss/server/default/conf/bootstrap/profile.xml

# Configure external deployment folder
RUN sed -i 's/<value>${jboss.server.home.url}deploy<\/value>/<value>${jboss.server.home.url}deploy<\/value><value>file:\/\/\/app<\/value>/g' /opt/jboss/server/default/conf/bootstrap/profile.xml

# Patch JBoss Server with a new commons-collection lib
RUN rm -f /opt/jboss/common/lib/commons-collections.jar
ADD ./patches/libs/commons-collections-3.2.1.jar /opt/jboss/common/lib/

ENV JBOSS_HOME /opt/jboss

CMD ["/opt/jboss/bin/run.sh", "-b", "0.0.0.0"]

EXPOSE 8080
EXPOSE 8009
